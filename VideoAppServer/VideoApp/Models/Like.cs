﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class Like
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        public bool IsLike { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public Video Video { get; set; }
        [ForeignKey("Video")]
        public int? VideoId { get; set; }

        public Comment Comment { get; set; }
        [ForeignKey("Comment")]
        public int? CommentId { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
    }
}