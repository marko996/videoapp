﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class Subscription
    {
        public int Id { get; set; }
        public string SubscriberUsername { get; set; }
        public string SubscribedUsername { get; set; }

    }
}