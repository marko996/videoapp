﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class Filter
    {
        public string Name { get; set; }
        public string User { get; set; }
        public int? ViewsStart { get; set; }
        public int? ViewsStop { get; set; }
        public DateTime DateStart { get; set; }
        public DateTime DateStop { get; set; }
        public string OrderBy { get; set; }

    }
}