﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class UserDTO
    {
        public string UserName { get; set; }
        public int Subscribers { get; set; }
        public string Date { get; set; }
        public string Description { get; set; }
        public bool Baned { get; set; }
        public byte[] Picture { get; set; }
    }
}