﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public enum Visibility
    {
        Public,
        Unlisted,
        Private

    }
    public class Video
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        public string VideoURL { get; set; }
        [Required]
        public string Thumbnail { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [Required]
        public Visibility Visibility { get; set; }
        [Required]
        public bool EnableComments { get; set; }
        [Required]
        public bool EnableRating { get; set; }
        [Required]
        public bool Blocked { get; set; }
        [Required]
        public int Views { get; set; }
        [Required]
        public DateTime Date { get; set; }

        public User User { get; set; }
        [ForeignKey("User")]
        public string UserId { get; set; }

        public virtual IEnumerable<Like> Likes { get; set; }
        public virtual IEnumerable<Comment> Comments { get; set; }

    }
}