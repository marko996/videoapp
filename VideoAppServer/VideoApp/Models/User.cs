﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class User : ApplicationUser
    {
        [StringLength(20, MinimumLength = 2)]
        public string Name { get; set; }
        [StringLength(20, MinimumLength = 2)]
        public string LastName { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public bool Baned { get; set; }
        public string PicturePath { get; set; }
        [NotMapped]
        public  IEnumerable<User> Subrsibers { get; set; }

        [NotMapped]
        public  IEnumerable<Like> VideoLikes { get; set; }

        [NotMapped]
        public  IEnumerable<Comment> Comments { get; set; }

    }
}