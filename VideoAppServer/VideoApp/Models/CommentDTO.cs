﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string Date { get; set; }
        public string User { get; set; }
        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public byte[] UserPicture { get; set; }
    }
}