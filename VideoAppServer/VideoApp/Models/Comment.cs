﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class Comment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        [Required]
        [StringLength(500)]
        public string Content { get; set; }
        [Required]
        public DateTime Date { get; set; }
        public User User { get; set; }
        [Required]
        public string UserId { get; set; }
        public Video Video { get; set; }
        [Required]
        [ForeignKey("Video")]
        public int VideoId { get; set; }
        public virtual IEnumerable<Like> Likes { get; set; }
    }
}