﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class VideoDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Thumbnail { get; set; }
        public int Views { get; set; }
        public string Date { get; set; }
        public string User { get; set; }
        public byte[] UserPicture { get; set; }
    }
}