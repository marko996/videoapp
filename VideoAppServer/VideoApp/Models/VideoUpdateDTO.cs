﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class VideoUpdateDTO
    {   
        [Required]
        public int Id { get; set; }
 
        public string Description { get; set; }
        [Required]
        public Visibility Visibility { get; set; }
        [Required]
        public bool EnableComments { get; set; }
        [Required]
        public bool EnableRating { get; set; }

        public bool? Blocked { get; set; }
    }
}