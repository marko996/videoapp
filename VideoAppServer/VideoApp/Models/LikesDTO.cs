﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class LikesDTO
    {
        public int Likes { get; set; }
        public int Dislikes { get; set; }
    }
}