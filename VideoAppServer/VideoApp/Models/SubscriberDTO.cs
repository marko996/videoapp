﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class SubscriberDTO
    {
        public string Subscribed { get; set; }
        public int Subscribers { get; set; }
    }
}