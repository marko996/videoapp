﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VideoApp.Models
{
    public class VideoGetByIdDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string VideoURL { get; set; }

        public string Description { get; set; }

        public bool EnableComments { get; set; }

        public bool EnableRating { get; set; }
        public Visibility Visibility { get; set; }
        public bool Blocked { get; set; }

        public int Views { get; set; }

        public string Date { get; set; }

        public string User { get; set; }

        public int Likes { get; set; }
        public int Dislikes { get; set; }
        public byte[] UserPicture { get; set; }

    }
}