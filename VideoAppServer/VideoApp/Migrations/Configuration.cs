namespace VideoApp.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.IO;
    using System.Linq;
    using VideoApp.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<VideoApp.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(VideoApp.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            roleManager.Create(new IdentityRole("Administrator"));


            context.SaveChanges();

            var user1 = new User { Email = "mirko@gmail.com", UserName = "mirko1", Name = "Mirko", LastName = "Mirkovic", Baned = false, Date = DateTime.Now,PicturePath= @"\download.jpg" };
            manager.Create(user1, "Sifra.123");

            context.SaveChanges();

            var user2 = new User { Email = "nikola@gmail.com", UserName = "nikola23", Name = "Nikola", LastName = "Nikolic", Baned = false, Date = DateTime.Now, PicturePath = @"\download.jpg" };
            manager.Create(user2, "Sifra.123");

            context.SaveChanges();

            var user3 = new User { Email = "milica@gmail.com", UserName = "milica5", Name = "Milica", LastName = "Milicic", Baned = false, Date = DateTime.Now, PicturePath = @"unnamed.jpg" };
            manager.Create(user3, "Sifra.123");

            context.SaveChanges();

            var user4 = new User { Email = "jovana@gmail.com", UserName = "jovana19", Name = "Jovana", LastName = "Jovanovic", Baned = false, Date = DateTime.Now, PicturePath = @"\unnamed.jpg" };
            manager.Create(user4, "Sifra.123");


            context.SaveChanges();

            var admin = context.Users.FirstOrDefault(x => x.UserName == "mirko1");

            manager.AddToRole(admin.Id, "Administrator");


            context.Videos.AddOrUpdate(new Video()
            {
                Id = 1,
                Name = "NBA Top 10",
                VideoURL = "https://www.youtube.com/embed/RkCZuYzzGoM?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://fosmedia.me/sites/default/files/styles/large/public/slike/2018/04/maxresdefault_8.jpg?itok=T-GryJVf",
                Visibility = Visibility.Unlisted,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = DateTime.Now,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id
            });

            context.Videos.AddOrUpdate(new Video()
            {
                Id = 2,
                Name = "React Js",
                VideoURL = "https://www.youtube.com/embed/DLX62G4lc44?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://www.webrexstudio.com/wp-content/uploads/2019/05/react-js-image.png",
                Visibility = Visibility.Public,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = new DateTime(2018, 7, 20),
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id
            });

            context.Videos.AddOrUpdate(new Video()
            {
                Id = 3,
                Name = "Khabib Nurmagomedov vs Conor McGregor",
                VideoURL = "https://www.youtube.com/embed/iu6n_Xja8mU?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://i.ytimg.com/vi/iu6n_Xja8mU/maxresdefault.jpg",
                Visibility = Visibility.Public,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = new DateTime(2019, 3, 6),
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id
            });

            context.Videos.AddOrUpdate(new Video()
            {
                Id = 4,
                Name = "Bob The Builder Season 3 Episode 1",
                VideoURL = "https://www.youtube.com/embed/8kFfHnre85o?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://ichef.bbci.co.uk/images/ic/1200x675/p06sdvzs.jpg",
                Visibility = Visibility.Public,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = new DateTime(2020, 3, 1),
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id
            });

            context.Videos.AddOrUpdate(new Video()
            {
                Id = 5,
                Name = "ASP.NET Web Api",
                VideoURL = "https://www.youtube.com/embed/vN9NRqv7xmY?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://estradaci.com/wp-content/uploads/2019/01/ASP.Net-Web-API-e1546465931531.jpg",
                Visibility = Visibility.Public,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = new DateTime(2018, 6, 11),
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id
            });

            context.Videos.AddOrUpdate(new Video()
            {
                Id = 6,
                Name = "Novak Djokovic beast mode tennis",
                VideoURL = "https://www.youtube.com/embed/4iTiRvk4FHY?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://rs.n1info.com/Picture/300385/jpeg/Novak-Djokovic",
                Visibility = Visibility.Public,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = new DateTime(2010, 5, 15),
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id
            });

            context.Videos.AddOrUpdate(new Video()
            {
                Id = 7,
                Name = "Best Places to Visit in Europe",
                VideoURL = "https://www.youtube.com/embed/0GZSfBuhf6Y?autoplay=1&mute=1&enablejsapi=1",
                Thumbnail = "https://www.nomadicmatt.com/wp-content/uploads/2018/08/5daysinparis1.jpg",
                Visibility = Visibility.Private,
                EnableComments = true,
                EnableRating = true,
                Blocked = false,
                Views = 0,
                Date = new DateTime(2008, 3, 1),
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 1,
                Content = "Awesome video!",
                Date = new DateTime(2020, 4, 17),
                VideoId = 1,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 2,
                Content = "I dont like it",
                Date = new DateTime(2020, 4, 16),
                VideoId = 1,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 3,
                Content = "It is very good",
                Date = new DateTime(2020, 4, 15),
                VideoId = 1,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 4,
                Content = "Awesome video!",
                Date = new DateTime(2019, 4, 17),
                VideoId = 2,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 5,
                Content = "I dont like it",
                Date = new DateTime(2018, 9, 1),
                VideoId = 2,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 6,
                Content = "It is very good",
                Date = new DateTime(2020, 3, 15),
                VideoId = 2,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 7,
                Content = "Awesome video!",
                Date = new DateTime(2019, 5, 17),
                VideoId = 3,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 8,
                Content = "I dont like it",
                Date = new DateTime(2020, 4, 16),
                VideoId = 3,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 9,
                Content = "It is very good",
                Date = new DateTime(2020, 1, 3),
                VideoId = 3,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 10,
                Content = "Awesome video!",
                Date = new DateTime(2020, 3, 1),
                VideoId = 4,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 11,
                Content = "I dont like it",
                Date = new DateTime(2020, 3, 21),
                VideoId = 4,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 12,
                Content = "It is very good",
                Date = new DateTime(2020, 4, 11),
                VideoId = 4,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 13,
                Content = "Awesome video!",
                Date = new DateTime(2018, 7, 17),
                VideoId = 5,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 14,
                Content = "I dont like it",
                Date = new DateTime(2020, 4, 12),
                VideoId = 5,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 15,
                Content = "It is very good",
                Date = new DateTime(2019, 7, 1),
                VideoId = 5,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 16,
                Content = "Awesome video!",
                Date = new DateTime(2011, 11, 3),
                VideoId = 6,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "nikola23").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 17,
                Content = "I dont like it",
                Date = new DateTime(2015, 7, 11),
                VideoId = 6,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "jovana19").Id,
            });

            context.Comments.AddOrUpdate(new Comment()
            {
                Id = 18,
                Content = "It is very good",
                Date = new DateTime(2017, 3, 19),
                VideoId = 6,
                UserId = context.Users.FirstOrDefault(x => x.UserName == "milica5").Id,
            });

            context.SaveChanges();

        }
    }
}
