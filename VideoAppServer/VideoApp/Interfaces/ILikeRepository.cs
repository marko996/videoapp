﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoApp.Models;

namespace VideoApp.Interfaces
{
    public interface ILikeRepository
    {
        LikesDTO LikeAndDislike(Like like);
    }
}
