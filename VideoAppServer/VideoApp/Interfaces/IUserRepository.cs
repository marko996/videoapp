﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoApp.Models;

namespace VideoApp.Interfaces
{
    public interface IUserRepository
    {
        UserDTO GetByUserName(string username);
        User GetByLoggedInUser(string id);
        void Update(UserEditDTO user);
        void AddOrUpdateUserPicture(string path,string id);
    }
}
