﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoApp.Models;

namespace VideoApp.Interfaces
{
    public interface ISubscriptionRepository
    {
        bool IsSubscriber(string id,string subscribed);
        void Create(string id,Subscription subscription);
        void Delete(string id,string subscribed);
        IEnumerable<SubscriberDTO> GetSubscribing(string user);
    }
}
