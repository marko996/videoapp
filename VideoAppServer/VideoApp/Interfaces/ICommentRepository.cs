﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoApp.Models;

namespace VideoApp.Interfaces
{
    public interface ICommentRepository
    {
        IEnumerable<Comment> GetByVideoId(int id);
        void Create(Comment comment);
    }
}