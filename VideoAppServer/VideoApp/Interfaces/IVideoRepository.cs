﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VideoApp.Models;

namespace VideoApp.Interfaces
{
    public interface IVideoRepository
    {
        IEnumerable<Video> GetAll();
        IEnumerable<Video> Filter(Filter filter);
        IEnumerable<Video> GetByUser(string user);
        Video GetById(int id);
        void Update(VideoUpdateDTO video);
        void Delete(Video video);
        void AddView(int videoId);
        void Create(Video video);
    }
}
