﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using VideoApp.Interfaces;
using VideoApp.Models;
using Microsoft.AspNet.Identity;
using System.IO;
using VideoApp.Methods;

namespace VideoApp.Controllers
{
    public class VideosController : ApiController
    {
        IVideoRepository _repository { get; set; }

        public VideosController(IVideoRepository repository)
        {
            _repository = repository;
        }


        public IEnumerable<VideoDTO> GetAll()
        {
            if (User.IsInRole("Administrator"))
            {
                var videosAdmin = _repository.GetAll().Select(x => new VideoDTO()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Thumbnail = x.Thumbnail,
                    User = x.User.UserName,
                    Views = x.Views,
                    Date = HelpMethods.DateTimeToString(x.Date),
                    UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
                });

                return videosAdmin;
            }
            
            var videosUser = _repository.GetAll().Where(x => ((x.Visibility != Visibility.Private & !x.Blocked) || x.User == User)).Select(x => new VideoDTO() 
            { 
                Id = x.Id,
                Name = x.Name,
                Thumbnail = x.Thumbnail,
                User = x.User.UserName,
                Views = x.Views,
                Date = HelpMethods.DateTimeToString(x.Date),
                UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
            });
   
            return videosUser;
        }


        [Route("api/Filter")]
        public IEnumerable<VideoDTO> PostFilter(Filter filter)
        {

            if (User.IsInRole("Administrator"))
            {
                var videosAdministrator = _repository.Filter(filter).Select(x => new VideoDTO()
                { 
                    Id = x.Id,
                    Name = x.Name,
                    Thumbnail = x.Thumbnail,
                    User = x.User.UserName,
                    Views = x.Views,
                    Date = HelpMethods.DateTimeToString(x.Date),
                    UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
                });

                return videosAdministrator;
            }

            var videosUser = _repository.Filter(filter).Where(x => ((x.Visibility != Visibility.Private & !x.Blocked) || x.User == User)).Select(x => new VideoDTO() 
            { 
                Id = x.Id,
                Name = x.Name,
                Thumbnail = x.Thumbnail,
                User = x.User.UserName,
                Views = x.Views,
                Date = HelpMethods.DateTimeToString(x.Date),
                UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
            });

            return videosUser;
        }
        [ResponseType(typeof(VideoGetByIdDTO))]
        public IHttpActionResult GetById(int id)
        {
            var video = _repository.GetById(id);

            if(video == null)
            {
                return NotFound();
            }

            var videoDTO = new VideoGetByIdDTO()
            {
                Id = video.Id,
                Name = video.Name,
                User = video.User.UserName,
                VideoURL = video.VideoURL,
                Date = HelpMethods.DateTimeToString(video.Date),
                Description = video.Description,
                EnableComments = video.EnableComments,
                EnableRating = video.EnableRating,
                Visibility = video.Visibility,
                Blocked = video.Blocked,
                Views = video.Views + 1,
                Likes = video.EnableRating ? video.Likes.Where(x => x.IsLike == true).Count() : 0,
                Dislikes = video.EnableRating ? video.Likes.Where(x => x.IsLike == false).Count() : 0,
                UserPicture = HelpMethods.GetPictureFromPath(video.User.PicturePath)
            };

            if (User.IsInRole("Administrator"))
            {
                _repository.AddView(video.Id);

                return Ok(videoDTO);
            }

            if((video.Visibility == Visibility.Private & video.UserId != User.Identity.GetUserId()) || (video.Blocked & video.UserId != User.Identity.GetUserId()))
            {
                return Unauthorized();
            }

            if(User.Identity.GetUserId() != video.UserId)
            {
                _repository.AddView(video.Id);
            }

            return Ok(videoDTO);
        }

        [Authorize]
        public IHttpActionResult DeleteVideo(int id)
        {
            var video = _repository.GetById(id);

            if(video == null)
            {
                return NotFound();
            }

            if(!(User.IsInRole("administrator") || User.Identity.GetUserId() == video.User.Id))
            {
                return Unauthorized();
            }

            _repository.Delete(video);

            return Ok();
            
        }

        [Authorize]
        public IHttpActionResult PutVideo(int id,VideoUpdateDTO video)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != video.Id)
            {
                return BadRequest();
            }

            if (!User.IsInRole("Administrator"))
            {
                video.Blocked = null;
            }

            _repository.Update(video);

            return Ok(video);
        }
        [Route("api/VideosByUser")]
        public IEnumerable<VideoDTO> GetByUser(string user)
        {
            if (User.IsInRole("Administrator"))
            {
                var videosAdministrator = _repository.GetByUser(user).Select(x => new VideoDTO() 
                { 
                    Id = x.Id,
                    Name = x.Name,
                    Thumbnail = x.Thumbnail,
                    User = x.User.UserName,
                    Views = x.Views,
                    Date = HelpMethods.DateTimeToString(x.Date),
                    UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
                });

                return videosAdministrator;
            }

            var videosUser = _repository.GetByUser(user).Where(x => ((x.Visibility != Visibility.Private & !x.Blocked) || x.User == User)).Select(x => new VideoDTO() 
            { 
                Id = x.Id,
                Name = x.Name,
                Thumbnail = x.Thumbnail,
                User = x.User.UserName,
                Views = x.Views,
                Date = HelpMethods.DateTimeToString(x.Date),
                UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
            });


            return videosUser;
        }
        [Authorize]
        [ResponseType(typeof(Video))]
        public IHttpActionResult PostVideo(Video video)
        {
            video.UserId = User.Identity.GetUserId();

            video.VideoURL = video.VideoURL.Replace("watch?v=", "embed/");

            video.VideoURL = video.VideoURL + "?autoplay=1&mute=1&enablejsapi=1";

            try
            {
                _repository.Create(video);

                return CreatedAtRoute("DefaultApi", new { id = video.Id }, video);
            }
            catch
            {
                return BadRequest();
            }
        }
    }
}
