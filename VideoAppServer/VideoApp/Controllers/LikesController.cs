﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VideoApp.Interfaces;
using VideoApp.Models;
using Microsoft.AspNet.Identity;

namespace VideoApp.Controllers
{
    public class LikesController : ApiController
    {
        ILikeRepository _repository { get; set; }

        public LikesController(ILikeRepository repository)
        {
            _repository = repository;
        }
        [Authorize]
        public IHttpActionResult PostLike(Like like)
        {
            like.UserId = User.Identity.GetUserId();

            like.Date = DateTime.Now;

          
                var likesAndDislikes =_repository.LikeAndDislike(like);

                return Ok(likesAndDislikes);
          

        }
    }
}
