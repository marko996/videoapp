﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using VideoApp.Interfaces;
using VideoApp.Models;

namespace VideoApp.Controllers
{
    public class UsersController : ApiController
    {
        IUserRepository _repository { get; set; }

        public UsersController(IUserRepository repository)
        {
            _repository = repository;
        }

        [ResponseType(typeof(UserDTO))]
        public IHttpActionResult GetByUsername(string username)
        {
            try
            {
                var user = _repository.GetByUserName(username);

                return Ok(user);
            }
            catch
            {
                return BadRequest();
            }
        }
        [Authorize]
        [ResponseType(typeof(User))]
        public IHttpActionResult GetUserInformations()
        {
            var user = _repository.GetByLoggedInUser(User.Identity.GetUserId());

            return Ok(new UserEditDTO() 
            { 
                Id = user.Id,
                Description = user.Description,
                Name = user.Name,
                LastName = user.LastName
            });
        }
        [Authorize]
        public IHttpActionResult PutUser(UserEditDTO user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if(user.Id != User.Identity.GetUserId())
            {
                return Unauthorized();
            }

            try
            {
                _repository.Update(user);

                return Ok();
            }
            catch
            {
                return InternalServerError();
            }
            
        }

        [Authorize]
        [Route("api/users/PostUserImage")]
        public async Task<IHttpActionResult> PostImage()
        {
            var root = HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);

                foreach (var file in provider.FileData)
                {
                    var name = file.Headers.ContentDisposition.FileName;

                    name = name.Trim('"');

                    var localFileName = file.LocalFileName;
                    var filePath = Path.Combine(root, name);

                    _repository.AddOrUpdateUserPicture(@"\" + name, User.Identity.GetUserId());

                    File.Move(localFileName, filePath);
                }
            }
            catch
            {
                return BadRequest();
            }

            return Ok();

            //var root = @"E:\VisualStudio\VideoApp\Images";
            //var provider = new MultipartFormDataStreamProvider(root);

            //try
            //{
            //    await Request.Content.ReadAsMultipartAsync(provider);

            //    foreach (var file in provider.FileData)
            //    {
            //        var name = file.Headers.ContentDisposition.FileName;

            //        name = name.Trim('"');

            //        var localFileName = file.LocalFileName;
            //        var filePath = Path.Combine(root, name);

            //        _repository.AddOrUpdateUserPicture(filePath, User.Identity.GetUserId());

            //        File.Move(localFileName, filePath);
            //    }
            //}
            //catch
            //{
            //    return BadRequest();
            //}

            //return Ok();

        }
    }
}
