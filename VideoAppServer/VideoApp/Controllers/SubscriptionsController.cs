﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VideoApp.Interfaces;
using VideoApp.Models;


namespace VideoApp.Controllers
{
    public class SubscriptionsController : ApiController
    {
        ISubscriptionRepository _repository { get; set; }

        public SubscriptionsController(ISubscriptionRepository repository)
        {
            _repository = repository;
        }
        [Authorize]
        public IHttpActionResult GetIsSubscriber(string subscribed)
        {
            var id = User.Identity.GetUserId();
       
            var isSubscriber = _repository.IsSubscriber(id,subscribed);

            return Ok(isSubscriber);
        }

        [Authorize]
        public IHttpActionResult PostSubscription(Subscription subscription)
        {
            var id = User.Identity.GetUserId();

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.Create(id,subscription);

                return CreatedAtRoute("DefaultApi", new { Id = subscription.Id }, subscription);
            }
            catch
            {
                return BadRequest();
            }
            
        }

        [Authorize]
        public IHttpActionResult DeleteSubscription(string subscribed)
        {
            var id = User.Identity.GetUserId();

            try
            {
                _repository.Delete(id,subscribed);

                return Ok();
            }
            catch
            {
                return BadRequest();
            }
        }

        public IEnumerable<SubscriberDTO> GetSubscribing(string user)
        {
            return _repository.GetSubscribing(user);
        }
      
    }
}
