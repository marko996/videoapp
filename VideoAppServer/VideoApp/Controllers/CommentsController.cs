﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Description;
using VideoApp.Interfaces;
using VideoApp.Models;
using Microsoft.AspNet.Identity;
using VideoApp.Methods;
using System.IO;

namespace VideoApp.Controllers
{
    public class CommentsController : ApiController
    {
        ICommentRepository _repository { get; set; }

        public CommentsController(ICommentRepository repository)
        {
            _repository = repository;
        }
        [Route("api/CommentsByVideo")]
        public IEnumerable<CommentDTO> GetByVideoId(int id)
        {
            var comments = _repository.GetByVideoId(id);

            var commentsDTO = comments.Select(x => new CommentDTO()
            {
                Id = x.Id,
                Date = HelpMethods.DateTimeToString(x.Date),
                Content = x.Content,
                User = x.User.UserName,
                Likes = x.Likes.Where(y => y.IsLike == true).Count(),
                Dislikes = x.Likes.Where(y => y.IsLike == false).Count(),
                UserPicture = HelpMethods.GetPictureFromPath(x.User.PicturePath)
            });

            return commentsDTO;
        }
        [Authorize]
        [ResponseType(typeof(CommentDTO))]
        public IHttpActionResult PostComment(Comment comment)
        {

            comment.UserId = User.Identity.GetUserId();

            try
            {
                _repository.Create(comment);

                return CreatedAtRoute("DefaultApi", new { id = comment.Id }, comment);
            }
            catch
            {
                return BadRequest();
            }

        }
    }
}
