﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using VideoApp.Interfaces;
using VideoApp.Models;

namespace VideoApp.Repository
{
    public class VideoRepository : IDisposable,IVideoRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Video> GetAll()
        {
            return db.Videos.Include(x => x.User);
        }

        public IEnumerable<Video> Filter(Filter filter)
        {
            var videos = db.Videos.Include(x => x.User);

            if(filter.Name != null & filter.Name != "")
            {
                videos = videos.Where(x => x.Name.Contains(filter.Name));
            }

            if(filter.User != null & filter.User != "")
            {
                videos = videos.Where(x => x.User.UserName.Contains(filter.User));
            }

            if (filter.ViewsStart != null)
            {
                videos = videos.Where(x => x.Views > filter.ViewsStart);
            }

            if (filter.ViewsStop != null)
            {
                videos = videos.Where(x => x.Views < filter.ViewsStop);
            }

            if (filter.DateStart != null & filter.DateStart.Year > 2000)
            {
                videos = videos.Where(x => x.Date > filter.DateStart);
            }

            if (filter.DateStop != null & filter.DateStop.Year > 2000)
            {
                videos = videos.Where(x => x.Date < filter.DateStop);
            }

            if(filter.OrderBy == "Name")
            {
                videos = videos.OrderBy(x => x.Name);
            }

            if (filter.OrderBy == "NameDesc")
            {
                videos = videos.OrderByDescending(x => x.Name);
            }

            if (filter.OrderBy == "User")
            {
                videos = videos.OrderBy(x => x.User.UserName);
            }

            if (filter.OrderBy == "UserDesc")
            {
                videos = videos.OrderByDescending(x => x.User.UserName);
            }

            if (filter.OrderBy == "Views")
            {
                videos = videos.OrderBy(x => x.Views);
            }

            if (filter.OrderBy == "ViewsDesc")
            {
                videos = videos.OrderByDescending(x => x.Views);
            }

            if (filter.OrderBy == "Date")
            {
                videos = videos.OrderBy(x => x.Date);
            }

            if (filter.OrderBy == "DateDesc")
            {
                videos = videos.OrderByDescending(x => x.Date);
            }

            return videos;
        }

        public Video GetById(int id)
        {
            var video = db.Videos.Include(x => x.User).FirstOrDefault(x => x.Id == id);

            video.Likes = db.Likes.Where(x => x.VideoId == id);

            return video;
        }

        public void AddView(int videoId)
        {
            db.Videos.Find(videoId).Views++;

            db.SaveChanges();
        }

        public void Delete(Video video)
        {
            db.Videos.Remove(video);

            db.SaveChanges();
        }

        public void Update(VideoUpdateDTO video)
        {
            var videoToUpdate = db.Videos.Find(video.Id);

            videoToUpdate.Visibility = video.Visibility;
            videoToUpdate.EnableComments = video.EnableComments;
            videoToUpdate.EnableRating = video.EnableRating;
            videoToUpdate.Description = video.Description;
            
            if(video.Blocked != null)
            {
                videoToUpdate.Blocked = (bool)video.Blocked;
            }

            db.Entry(videoToUpdate).State = EntityState.Modified;

            db.SaveChanges();
        }

        public IEnumerable<Video> GetByUser(string user)
        {
            return db.Videos.Where(x => x.User.UserName == user).Include(x => x.User);
        }

        public void Create(Video video)
        {
            var id = db.Videos.Max(x => x.Id);

            video.Id = id + 1;

            video.Date = DateTime.Now;

            video.Blocked = false;

            db.Videos.Add(video);

            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}