﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using VideoApp.Interfaces;
using VideoApp.Methods;
using VideoApp.Models;

namespace VideoApp.Repository
{
    public class UserRepository : IDisposable, IUserRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();
     
        public UserDTO GetByUserName(string username)
        {
            var user = db.Users.First(x => x.UserName == username) as User;

            if(user == null)
            {
                throw new Exception();
            }        

            var userDTO = new UserDTO()
            {
                UserName = user.UserName,
                Date = user.Date.Year.ToString() + "." + user.Date.Month.ToString() + "." + user.Date.Day.ToString(),
                Baned = user.Baned,
                Description = user.Description,
                Subscribers = db.Subscriptions.Where(x => x.SubscribedUsername == username).Count(),
                Picture = HelpMethods.GetPictureFromPath(user.PicturePath)
            };

            return userDTO;
        }

        public User GetByLoggedInUser(string id)
        {
            return db.Users.Find(id) as User;
        }

        public void Update(UserEditDTO user)
        {
            var userToUpdate = db.Users.Find(user.Id) as User;

            userToUpdate.Name = user.Name;
            userToUpdate.LastName = user.LastName;
            userToUpdate.Description = user.Description;

            try
            {
                db.Entry(userToUpdate).State = EntityState.Modified;

                db.SaveChanges();
            }
            catch
            {
                throw;
            }

            
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void AddOrUpdateUserPicture(string path,string id)
        {
            var user = db.Users.Find(id) as User;

            var oldPicture = user.PicturePath;

            try
            {
                File.Delete(oldPicture);

                user.PicturePath = path;

                db.SaveChanges();
            }
            catch
            {
                user.PicturePath = path;

                db.SaveChanges();
            }
        }
    }
}