﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoApp.Interfaces;
using VideoApp.Models;

namespace VideoApp.Repository
{
    public class LikeRepository : IDisposable,ILikeRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public LikesDTO LikeAndDislike(Like like)
        {
            var testLike = db.Likes.FirstOrDefault(x => x.UserId == like.UserId & (x.VideoId == like.VideoId & x.CommentId == like.CommentId));

            LikesDTO likesDTO = new LikesDTO();

            if (testLike == null)
            {
                if(db.Likes.Count() != 0)
                {
                    var id = db.Likes.Max(x => x.Id);

                    like.Id = id + 1;
                }

                db.Likes.Add(like);

                db.SaveChanges();

                if(like.VideoId != null)
                {
                    likesDTO.Likes = db.Likes.Where(x => x.VideoId == like.VideoId & x.IsLike == true).Count();

                    likesDTO.Dislikes = db.Likes.Where(x => x.VideoId == like.VideoId & x.IsLike == false).Count();
                }
                else
                {
                    likesDTO.Likes = db.Likes.Where(x => x.CommentId == like.CommentId & x.IsLike == true).Count();

                    likesDTO.Dislikes = db.Likes.Where(x => x.CommentId == like.CommentId & x.IsLike == false).Count();
                }
         

                return likesDTO;
            }

            if(testLike.IsLike != like.IsLike)
            {
                db.Likes.Remove(testLike);

                db.SaveChanges();

                if (like.VideoId != null)
                {
                    likesDTO.Likes = db.Likes.Where(x => x.VideoId == like.VideoId & x.IsLike == true).Count();

                    likesDTO.Dislikes = db.Likes.Where(x => x.VideoId == like.VideoId & x.IsLike == false).Count();
                }
                else
                {
                    likesDTO.Likes = db.Likes.Where(x => x.CommentId == like.CommentId & x.IsLike == true).Count();

                    likesDTO.Dislikes = db.Likes.Where(x => x.CommentId == like.CommentId & x.IsLike == false).Count();
                }

                return likesDTO;

            }

            if (like.VideoId != null)
            {
                likesDTO.Likes = db.Likes.Where(x => x.VideoId == like.VideoId & x.IsLike == true).Count();

                likesDTO.Dislikes = db.Likes.Where(x => x.VideoId == like.VideoId & x.IsLike == false).Count();
            }
            else
            {
                likesDTO.Likes = db.Likes.Where(x => x.CommentId == like.CommentId & x.IsLike == true).Count();

                likesDTO.Dislikes = db.Likes.Where(x => x.CommentId == like.CommentId & x.IsLike == false).Count();
            }

            return likesDTO;

        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}