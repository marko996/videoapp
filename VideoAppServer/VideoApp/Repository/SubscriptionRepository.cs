﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using VideoApp.Interfaces;
using VideoApp.Models;

namespace VideoApp.Repository
{
    public class SubscriptionRepository : IDisposable,ISubscriptionRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public bool IsSubscriber(string id, string subscribed)
        {
            var subscriber = db.Users.Find(id).UserName;

            var subscription = db.Subscriptions.FirstOrDefault(x => x.SubscriberUsername == subscriber & x.SubscribedUsername == subscribed);

            if (subscription == null)
            {
                return false;
            }

            return true;
        }

        public void Create(string id,Subscription subscription)
        {
            var subscriber = db.Users.Find(id).UserName;

            subscription.SubscriberUsername = subscriber;

            if(!(db.Users.Any(x => x.UserName == subscription.SubscribedUsername)))
            {
                throw new Exception();
            }

            db.Subscriptions.Add(subscription);

            db.SaveChanges();
        }

        public void Delete(string id,string subscribed)
        {
            var subscriber = db.Users.Find(id).UserName;

            var subscription =  db.Subscriptions.FirstOrDefault(x => x.SubscriberUsername == subscriber & x.SubscribedUsername == subscribed);

            if(subscription == null)
            {
                throw new Exception();
            }

            db.Subscriptions.Remove(subscription);

            db.SaveChanges();
        }

        public IEnumerable<SubscriberDTO> GetSubscribing(string user)
        {
            return db.Subscriptions.Where(x => x.SubscriberUsername == user).Select(x => new SubscriberDTO() { Subscribed = x.SubscribedUsername,Subscribers = db.Subscriptions.Where(y => y.SubscribedUsername == x.SubscribedUsername).Count()});
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}