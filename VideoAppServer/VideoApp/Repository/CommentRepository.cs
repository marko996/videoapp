﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using VideoApp.Interfaces;
using VideoApp.Models;


namespace VideoApp.Repository
{
    public class CommentRepository : IDisposable, ICommentRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        public IEnumerable<Comment> GetByVideoId(int id)
        {
            if(db.Videos.Find(id).EnableComments == false)
            {
                return null;
            }

            var comments = db.Comments.Include(x => x.User).Where(x => x.VideoId == id);

            foreach (Comment c in comments)
            {
                c.Likes = db.Likes.Where(x => x.CommentId == c.Id);
            }

            return comments.OrderByDescending(x => x.Date);
        }

        public void Create(Comment comment)
        {
            var id =  db.Comments.Max(x => x.Id);

            comment.Id = id + 1;

            comment.Date = DateTime.Now;

            db.Comments.Add(comment);

            db.SaveChanges();
        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

       
    }
}