﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace VideoApp.Methods
{
    public class HelpMethods
    {
        public static string DateTimeToString(DateTime date)
        {
            return date.Day.ToString() + "." + date.Month.ToString() + "." + date.Year.ToString();
        }

        public static byte[] GetPictureFromPath(string path)
        {
            byte[] picture = null;

            try
            {
                picture = File.ReadAllBytes(HttpContext.Current.ApplicationInstance.Server.MapPath("~/App_Data")+path);

                return picture;
            }
            catch
            {
                return picture;
            }
        }
    }
}