import React from "react"
import Like from "./Like"
import Picture from "./Picture"

class Comment extends React.Component {

    render(){
        return(
            <div>
                <div style={{paddingLeft:"20px"}}>
                    <div style={{float:"left"}}>
                        <Picture data={this.props.userPicture} height="30px" width="30px" radius="50%"/>
                    </div>
                    <h4 style={{marginLeft:"40px",marginBottom:"30px"}}>
                        <a href={"/user/"+this.props.user}>
                            {this.props.user} 
                        </a> 
                    </h4>                   
                    <h5> {this.props.content} </h5>
                    <p>
                        Date:{this.props.date}
                        <span> {" "} </span>
                        <div style={{float:"right",marginRight:"30px"}}>
                            <Like like={true} commentId={this.props.id} liked = {this.props.liked}/>
                            <span> {" "} </span>
                            <span style={{color:"green"}}>  {this.props.likes} </span>
                            <span> {" "} </span>
                            <Like commentId={this.props.id} liked={this.props.liked}/>
                            <span> {" "} </span>
                            <span style={{color:"red"}}> {this.props.dislikes} </span>
                        </div>
                    </p>
                </div>
                <hr/>
             </div>
        )
    }
}

export default Comment