import React from "react"
import { loginFormStyle } from "./Styles"

class LoginForm extends React.Component {
    constructor(){
        super()
        this.state = {
            userName:"",
            password:""
        }
    }

    handleChange = (event) => {
        const {name,value} = event.target

        this.setState({ [name]:value })
    }

    handleSubmit = (event) => {
        event.preventDefault()

        fetch("https://localhost:44323/Token",{
            method:"POST",
            body: 'grant_type=password&username='+this.state.userName+'&password='+this.state.password
          })
          .then(response => response.json())
          .then(data => {
                if(data.access_token){
                    sessionStorage.setItem("token",data.access_token)
                    sessionStorage.setItem("user",data.userName)
                    
                    fetch("https://localhost:44323/api/Account/UserInfo",{
                        headers:{Authorization:"Bearer "+data.access_token}
                    })
                    .then(response => response.json())
                    .then(data => {
                        sessionStorage.setItem("role",data.Role)
                    }) 

                window.location.replace("/");
                }
                else{
                    alert("Login failed!")
                }
            })
    } 

    render(){
        return(
            <div>
                <div style = {loginFormStyle}>
                    <h2>Login form</h2>
                    <br/>
                    <form onSubmit = {this.handleSubmit} >
                        <div className="form-group">
                            <label>Username:</label>
                            <input  type="text"
                                    className="form-control"
                                    placeholder="Enter username"
                                    name="userName"
                                    onChange={this.handleChange}
                                    value={this.state.userName} 
                                    />
                        </div>
                        <div className="form-group">
                            <label>Password:</label>
                            <input  type="password"
                                    className="form-control"
                                    placeholder="Enter password"
                                    name="password"
                                    onChange={this.handleChange}
                                    value={this.state.password} 
                                    />
                        </div>
                        <button type="submit" className="btn btn-default">Log in</button>
                    </form>
                </div>
            </div>
        )
    }
}

export default LoginForm