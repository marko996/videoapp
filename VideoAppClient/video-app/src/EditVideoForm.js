import React from "react"
import axios from "axios"
import { editVideoFormStyle, editVideoFormStyle1 } from "./Styles"

class EditVideoPopUp extends React.Component {
    constructor() {
    super()
      this.state = {
        id:"",
        description:"",
        enableComments:"",
        enableRatings:"",
        visibility:"",
        blocked:""
      }
    }

    handleChange = (event) => {

      const {name,value,type,checked} = event.target

      type === "checkbox"? this.setState({[name]:checked}):this.setState({[name]:value})

    }

    componentDidMount() {
      this.setState(
        {
              id:this.props.id,
              description:(this.props.description? this.props.description:""),
              enableComments:this.props.enableComments,
              enableRatings:this.props.enableRatings,
              visibility:String(this.props.visibility),
              blocked:this.props.blocked}
        )}

    handleSaveChanges = (event) => {
      event.preventDefault()

      const data = {
                    Id:this.props.id,
                    Description: this.state.description,
                    Visibility: Number(this.state.visibility),
                    EnableComments: this.state.enableComments,
                    EnableRating: this.state.enableRatings
                  }

        axios("https://localhost:44323/api/Videos/"+this.props.id,{
                method:"PUT",
                data:data,
                headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}
        }).then((data) => this.props.closePopup(data.data))
        .catch(() => {alert("Saving changes failed!")})
    }

    handleDelete = (event) => {
        event.preventDefault()

        if(window.confirm("Are you sure?")){
                axios("https://localhost:44323/api/Videos/"+this.props.id,{
                  method:"DELETE",
                  headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}
          }).then(() => {window.location.replace("/")})
          .catch(() => {alert("Deleting video failed!")})
        }
    }

    render() {
      return (
        <div style={editVideoFormStyle}>
          <div style={editVideoFormStyle1}>
            <h3 style={{position:"relative",left:"35%"}}> Edit video </h3>
            <div style={{padding:"20px"}}>  
              <form>
                          <div className="form-group">
                              <label>Description:</label>
                              <textarea
                                      style={{resize:"none",height:"120px"}}
                                      className="form-control"
                                      name="description"
                                      onChange={this.handleChange}
                                      value={this.state.description}
                                      />
                          </div>
                          <div className="form-group">
                              <label> 
                              <input  type="checkbox"
                                      name="enableComments"
                                      onChange={this.handleChange}
                                      checked={this.state.enableComments}
                                      />
                                      Enable comments
                                      </label>
                          </div>
                          <br/>
                          <div className="form-group">
                              <label> 
                              <input  type="checkbox"
                                      name="enableRatings"
                                      onChange={this.handleChange}
                                      checked={this.state.enableRatings}
                                      />
                                      Enable ratings
                                </label>
                          </div>
                          <div className="form-group">
                              <br/>
                              <h4> Visibility: </h4>
                              <br/>
                              <label> 
                              <input  type="radio"
                                      name="visibility"
                                      onChange={this.handleChange}
                                      value="0"
                                      checked={this.state.visibility === "0"}
                                      />
                                      Public
                                </label>
                                <br/>
                                <label> 
                              <input  type="radio"
                                      name="visibility"
                                      onChange={this.handleChange}
                                      value="1"
                                      checked={this.state.visibility === "1"}
                                      />
                                      Unlisted
                                </label>
                                <br/>
                                <label> 
                              <input  type="radio"
                                      name="visibility"
                                      onChange={this.handleChange}
                                      value="2"
                                      checked={this.state.visibility === "2"}
                                      />
                                      Private
                                </label>
                          </div>
                          <br/>
                          {sessionStorage.getItem("role") === "Administrator"?
                          <div className="form-group">
                              <label> 
                              <input  type="checkbox"
                                      name="blocked"
                                      onChange={this.handleChange}
                                      checked={this.state.blocked}
                                      />
                                      Blocked
                                </label>
                              </div>: null
                          }
                          <label>
                            <button type="submit" className="btn btn-warning" onClick={this.handleSaveChanges}>Save changes</button>
                            <span> {" "} </span>
                            <button type="submit" className="btn btn-danger" onClick={this.handleDelete}>Delete</button>
                          </label>
                      </form>
                      <button className="btn btn-primary" style={{float:"right"}} onClick={this.props.togglePopup}> Cancel </button>
                    </div>
          
          </div>
        </div>
      )
    }
  }

export default EditVideoPopUp