import React from "react"
import axios from "axios"
import { addVideoFormStyle, addVideoFormStyle1 } from "./Styles"

class AddVideoForm extends React.Component{
    constructor(){
        super()
        this.state = {
            name:"",
            url:"",
            thumbnail:"",
            description:"",
            visibility:"0",
            enableComments:"",
            enableRatings:""
        }
    }

    handleChange = (event) => {

        const {name,value,type,checked} = event.target
  
        type === "checkbox"? this.setState({[name]:checked}):this.setState({[name]:value})
  
      }

      handleAdd = (event) => {
        event.preventDefault()

        const data = {
            Name:this.state.name,
            VideoURL:this.state.url,
            Thumbnail:this.state.thumbnail,
            Description: this.state.description,
            Visibility: Number(this.state.visibility),
            EnableComments: this.state.enableComments,
            EnableRating: this.state.enableRatings
          }

          axios("https://localhost:44323/api/Videos/",{
            method:"POST",
            data:data,
            headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}
    })
    .then((data) => this.props.togglePopup())
    .catch(() => {alert("Adding video failed!")})
    }

    render(){
          return (
            <div style={addVideoFormStyle}>
              <div style={addVideoFormStyle1}>
                <h3 style={{position:"relative",left:"40%"}}> Add video </h3>
                <div style={{padding:"20px"}}>  
                    <form>
                        <div className="form-group">
                                <label>Name:</label>
                                <input
                                        className="form-control"
                                        name="name"
                                        onChange={this.handleChange}
                                        value={this.state.name}
                                        />
                        </div>
                        <div className="form-group">
                                <label>URL:</label>
                                <input
                                        className="form-control"
                                        name="url"
                                        onChange={this.handleChange}
                                        value={this.state.url}
                                        />
                        </div>
                        <div className="form-group">
                                <label>Thumbnail:</label>
                                <input
                                        className="form-control"
                                        name="thumbnail"
                                        onChange={this.handleChange}
                                        value={this.state.thumbnail}
                                        />
                        </div>
                        <div className="form-group">
                                <label>Description:</label>
                                <textarea
                                        style={{resize:"none",height:"120px"}}
                                        className="form-control"
                                        name="description"
                                        onChange={this.handleChange}
                                        value={this.state.description}
                                        />
                        </div>
                        <div className="form-group">
                              <h4> Visibility: </h4>
                              <label> 
                              <input  type="radio"
                                      name="visibility"
                                      onChange={this.handleChange}
                                      value="0"
                                      checked={this.state.visibility === "0"}
                                      />
                                      Public
                                </label>
                                <br/>
                                <label> 
                              <input  type="radio"
                                      name="visibility"
                                      onChange={this.handleChange}
                                      value="1"
                                      checked={this.state.visibility === "1"}
                                      />
                                      Unlisted
                                </label>
                                <br/>
                                <label> 
                              <input  type="radio"
                                      name="visibility"
                                      onChange={this.handleChange}
                                      value="2"
                                      checked={this.state.visibility === "2"}
                                      />
                                      Private
                                </label>
                          </div>
                          <div className="form-group">
                              <label> 
                              <input  type="checkbox"
                                      name="enableComments"
                                      onChange={this.handleChange}
                                      checked={this.state.enableComments}
                                      />
                                      Enable comments
                                      </label>
                          </div>
                          <div className="form-group">
                              <label> 
                              <input  type="checkbox"
                                      name="enableRatings"
                                      onChange={this.handleChange}
                                      checked={this.state.enableRatings}
                                      />
                                      Enable ratings
                                </label>
                          </div>
                    </form>  
                    <br/> 
                    <button className="btn btn-success" onClick={this.handleAdd}>Add video</button>
                    <button className="btn btn-primary" style={{float:"right"}} onClick={this.props.togglePopup}> Cancel </button>            
                </div>
              
              </div>
            </div>
          
        )      
    }
}

export default AddVideoForm