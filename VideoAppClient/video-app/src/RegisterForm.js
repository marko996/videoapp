import React from "react"
import axios from "axios"
import {registerFormStyle} from "./Styles"


class RegisterForm extends React.Component{
    constructor(){
        super()
        this.state = {
            email:"",
            userName:"",
            password:"",
            passwordConfirm:"",
            name:"",
            lastName:""
        }
    }

    handleChange = (event) => {
        const {name,value} = event.target

        this.setState({ [name]:value })
    }

    handleSubmit = (event) => {
        event.preventDefault()

        const registerData = {
            Email: this.state.email,
            Password: this.state.password,
            ConfirmPassword: this.state.passwordConfirm,
            UserName: this.state.userName,
            Name: this.state.name,
            LastName: this.state.lastName
        } 

        axios("https://localhost:44323/api/Account/Register",{
            method:"POST",
            data:registerData
        }).then(() => {
            this.setState({email:"",userName:"",password:"",passwordConfirm:"",name:"",lastName:""})
            alert("Registered successfully!")                                
        })
        .catch(() => {
            alert("Registration failed!")
        })
    }


        render(){
             return(
                <div>
                    <div style = {registerFormStyle}>
                        <h2>Registration form</h2>
                        <br/>
                        <form onSubmit = {this.handleSubmit} >
                            <div className="form-group">
                                <label>Email address:</label>
                                <input  type="email"
                                        className="form-control"
                                        placeholder="Enter email"
                                        name="email"
                                        onChange={this.handleChange}
                                        value={this.state.email}
                                        />
                            </div>
                            <div className="form-group">
                                <label>Username:</label>
                                <input  type="text"
                                        className="form-control"
                                        placeholder="Enter username"
                                        name="userName"
                                        onChange={this.handleChange}
                                        value={this.state.userName} 
                                        />
                            </div>
                            <div className="form-group">
                                <label>Password:</label>
                                <input  type="password"
                                        className="form-control"
                                        placeholder="Enter password"
                                        name="password"
                                        onChange={this.handleChange}
                                        value={this.state.password} 
                                        />
                            </div>
                            <div className="form-group">
                                <label>Confirm Password:</label>
                                <input  type="password"
                                        className="form-control"
                                        placeholder="Confirm password"
                                        name="passwordConfirm"
                                        onChange={this.handleChange}
                                        value={this.state.passwordConfirm} 
                                        />
                            </div>
                            <div className="form-group">
                                <label>First Name:</label>
                                <input  type="text"
                                        className="form-control"
                                        placeholder="Enter first name"
                                        name="name"
                                        onChange={this.handleChange}
                                        value={this.state.name} 
                                        />
                            </div>
                            <div className="form-group">
                                <label>Last Name:</label>
                                <input  type="text"
                                        className="form-control"
                                        placeholder="Enter last name"              
                                        name="lastName"
                                        onChange={this.handleChange}
                                        value={this.state.lastName} 
                                        />
                            </div>
                            <button type="submit" className="btn btn-default">Register</button>
                        </form>
                    </div>
                </div>
        )
    }
}

export default RegisterForm