import React from "react"
import {Link} from "react-router-dom"
import Picture from "./Picture"
import { videoStyle, videoStyleDate,videoStyleViews,videoStyleUser, videoPictureStyle } from "./Styles"


class Video extends React.Component {
           
        render() {

            return(         
                        <div style={videoStyle}>
                            <Link to={"/video/"+this.props.id}>
                                <img alt="" width="280px" height="150" src={this.props.thumbnail} />
                                <p style={{color:"black"}}>
                                     <strong> {this.props.name} </strong>
                                </p>  
                            </Link>
                            <Link to={"/user/"+this.props.user}>
                                <div style={videoPictureStyle}>
                                    <Picture data={this.props.userPicture} height="50px" width="50px" radius="50%"/>
                                </div>
                            </Link>                           
                            <Link to={"/user/"+this.props.user}>
                                    <p style={videoStyleUser}> User: <u> {this.props.user} </u> </p>
                                </Link>
                                <p style={videoStyleViews}> Views: <u> {this.props.views} </u> </p>
                                <p style={videoStyleDate}> Date: <u> {this.props.date} </u> </p>
                        </div>
            )
        }

  }

export default Video