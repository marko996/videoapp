import React from "react"
import SubscribeButton from "./SubscribeButton"
import EditVideoForm from "./EditVideoForm"
import Like from "./Like"
import CommentsBox from "./CommentsBox"
import Picture from "./Picture"
import { videoSettingsStyle, videoDescriptionStyle,videoDescriptionStyle1, noDescriptionStyle } from "./Styles"


class VideoPage extends React.Component{
        constructor(){
            super()
            this.state = {
                id: "",
                name: "",
                videoURL: "",
                description: "",
                enableComments: "",
                enableRating: "",
                visibility:"",
                blocked:"",
                views: "",
                date: "",
                user: "",
                likes: "",
                dislikes: "",
                userPicture:"",
                token:sessionStorage.getItem("token"),
                showPopUp:false,
                mounted:false
            }
        }

        componentDidMount() {
            fetch("https://localhost:44323/api/Videos/"+this.props.match.params.id,{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
            .then(response => response.json())
            .then(data => {          
                this.setState({
                    id:data.Id,
                    name:data.Name,
                    videoURL:data.VideoURL,
                    description:data.Description,
                    enableComments:data.EnableComments,
                    enableRating:data.EnableRating,
                    views:data.Views,
                    visibility:data.Visibility,
                    blocked:data.Blocked,
                    date:data.Date,
                    user:data.User,
                    likes:data.Likes,
                    dislikes:data.Dislikes,
                    userPicture:data.UserPicture,
                    mounted:true
                })
            })
        }

        togglePopup = (childData) => {
            this.setState({
              showPopup: !this.state.showPopup
            })
          }

          closePopup = (childData) => {
                this.setState
                    ({
                        showPopup:false,
                        blocked:childData.Blocked,
                        description:childData.Description,
                        enableComments:childData.EnableComments,
                        enableRating:childData.EnableRating,
                        visibility:childData.Visibility
                    })
          }


        handleLike = (childData) => {
            this.setState({likes:childData.Likes,dislikes:childData.Dislikes})
        }
        
        render() {
            return(
                <div style={{position:"realtive"}}>
                    <CommentsBox enableComments={this.state.enableComments} videoId={this.props.match.params.id} />

                    <iframe style={{marginLeft:"20px"}}
                            width="55%" 
                            height="600px" 
                            title={this.state.name} 
                            src={this.state.videoURL} 
                            allowFullScreen> 
                    </iframe>

                    <div style={{marginLeft:"25px",width:"55%"}}>
                        {this.state.mounted && <SubscribeButton user={this.state.user}/>} 

                        {
                        (this.state.user === sessionStorage.getItem("user") || sessionStorage.getItem("role") === "Administrator")
                        && 
                        <button className="btn btn-warning"
                                onClick={this.togglePopup}
                                style={videoSettingsStyle}> 

                                Video settings

                        </button>
                        }
                        <div style={{float:"left"}}>
                            <Picture data={this.state.userPicture} height="80px" width="80px" radius="50%"/>
                        </div>
                        <div style={{marginLeft:"100px"}}>
                            <h2> {this.state.name} </h2>
                            <h4 style={{marginTop:"20px"}}>
                             User:
                            <a href={"/user/"+this.state.user}> {this.state.user} </a>
                            <span style={{marginLeft:"1%"}}> Views: {this.state.views} </span>
                            <span style={{marginLeft:"1%"}}> Date: {this.state.date} </span>

                            {
                            this.state.enableRating?
                                <span>
                                    <Like like = {true} videoId={this.state.id} liked = {this.handleLike} />                                  
                                    <span style={{marginLeft:"1%",color:"green"}}> {this.state.likes} </span>
                                    <Like videoId={this.state.id} liked = {this.handleLike}/>  
                                    <span style={{marginLeft:"1%",color:"red"}}> {this.state.dislikes} </span>
                                </span>
                                :
                                <span style={{marginLeft:"1%"}}> Ratings disabled. </span>
                            }
                        </h4>
                        </div>
                        <div style={videoDescriptionStyle}>
                             {
                                    
                                    this.state.description? 
                                    <div style={{videoDescriptionStyle1}}>
                                            {this.state.description}
                                    </div> 
                                    : 
                                    <h1 style = {noDescriptionStyle}> No description. </h1>
                                
                            } 
                        </div> 
                    </div>

                    { 
                        this.state.showPopup 
                        && 
                        <EditVideoForm 
                            id = {this.state.id}
                            description = {this.state.description}
                            visibility = {this.state.visibility}
                            enableComments = {this.state.enableRating}
                            enableRatings = {this.state.enableRating}
                            blocked={this.state.blocked}
                            closePopup={this.closePopup}
                            togglePopup={this.togglePopup}
                        />
                    }

                </div>
                  
                  
            )
        }
}

export default VideoPage