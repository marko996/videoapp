import React from "react"
import axios from "axios"
import { newCommentButtonStyle } from "./Styles"

class NewComment extends React.Component{
    constructor(){
        super()
        this.state = {
            content:""
        }
    }

    handleChange = (event) => {
        const {name,value} = event.target

        this.setState({[name]:value})
    }

    handleClick = (event) => {
        event.preventDefault()

        if(sessionStorage.getItem("token")) {
            const data = {
                VideoId:this.props.id,
                Content:this.state.content
            }
            axios("https://localhost:44323/api/Comments",{
                method:"POST",
                headers:{Authorization:"Bearer "+sessionStorage.getItem("token")},
                data:data
            }).then((data) => 
            {   
                this.props.commentAdded(true)
                this.setState({content:""})
            })
            .catch(() => {alert("Sending comment failed!")})
        }
        else{
            alert("You are not logged in!")
        }   
    }

    render(){
        return(
            <div>
                <div style={{paddingLeft:"20px"}}>
                    <h4 style={{color:"#A8A8A8"}}> Add comment: </h4>
                    <form className="form-group">
                        <textarea className="form-control"
                                  style={{height:"90px",width:"97%",resize:"none"}}
                                  name="content"
                                  value={this.state.content}
                                  onChange={this.handleChange}
                                  >
                        </textarea>
                        <button style={newCommentButtonStyle} className="btn btn-primary" onClick={this.handleClick}> Post comment </button>
                    </form>
                </div>
                <hr/>
             </div>
        )
    }
}

export default NewComment