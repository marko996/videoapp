import React from "react"
import RegisterForm from "./RegisterForm"
import LoginForm from "./LoginForm"
import HomePage from "./HomePage"
import VideoPage from "./VideoPage"
import UserPage from "./UserPage"
import NavigationBar from "./NavigationBar"
import {BrowserRouter as Router ,Switch, Route} from "react-router-dom"

class App extends React.Component{

    render(){
        return(
            <Router>     
                <NavigationBar />
                <Switch>
                    <Route path="/" exact component={HomePage}/>
                    <Route path="/register" component={RegisterForm} />
                    <Route path="/login" component={LoginForm}/>
                    <Route path="/video/:id" component={VideoPage} />
                    <Route path="/user/:id" component={UserPage}/>
                </Switch>             
            </Router>
        )
    }

}

export default App

