import React from "react"
import { subscribingStyle1, subscribingStyle2 } from "./Styles"

class Subscribing extends React.Component{
    
    render(){
        
        return(
                <div style={subscribingStyle1}>
                    <h3 style={{marginLeft:"20px"}}> 
                        <a href={"/user/"+this.props.user}> {this.props.user} </a>
                        <span style={subscribingStyle2}> Subscribers: {this.props.subscribers} </span>
                    </h3>
                    <hr/>
                </div>
        )
    }
}

export default Subscribing