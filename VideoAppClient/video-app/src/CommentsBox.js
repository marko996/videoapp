import React from "react"
import NewComment from "./NewComment"
import Comment from "./Comment"
import { commentsStyle,commentsStyle1, commentsDisabledStyle } from "./Styles"

class CommentsBox extends React.Component{
        constructor(){
            super()
            this.state = {
                comments:[]
            }
        }

        componentDidMount() {
                fetch("https://localhost:44323/api/CommentsByVideo?id="+this.props.videoId,{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
                .then(response => response.json())
                .then(data => {
                        this.setState({comments: data.map(x => <Comment key={x.Id} id={x.Id} content = {x.Content} user = {x.User} date={x.Date} likes = {x.Likes} dislikes={x.Dislikes} userPicture={x.UserPicture} liked={this.handleLike} />)})
                })
        }

        addComment = (childData) => {
            if(childData){
                    fetch("https://localhost:44323/api/CommentsByVideo?id="+this.props.videoId,{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
                    .then(response => response.json())
                    .then(data => {
                            this.setState({comments: data.map(x => <Comment key={x.Id} id={x.Id} content = {x.Content} user = {x.User} date={x.Date} likes = {x.Likes} dislikes={x.Dislikes} userPicture={x.UserPicture} liked={this.handleLike} />)})
                    })
            }
        } 

        handleLike = () => {
            this.componentDidMount()
        }


    render(){
        return(
                <div style={commentsStyle}>
                    {
                        this.props.enableComments?
                            <div style = {commentsStyle1}>
                                <hr/>
                                <NewComment id={this.props.videoId} commentAdded={this.addComment} />
                                {this.state.comments}
                            </div> 

                            :

                            <h1 style = {commentsDisabledStyle}> Comments disabled. </h1>
                        }
                </div>
        )
    }
}


export default CommentsBox

