import React from "react"

class NavigationBar extends React.Component {
    constructor() {
        super()
        this.state ={
            
        }
    }

    handleLogOut = (event) => {  
        if(sessionStorage.getItem("token")){

            event.preventDefault()

            sessionStorage.removeItem("token")
            sessionStorage.removeItem("user")
            sessionStorage.removeItem("role")

            window.location.reload()
        }
    }

    render() {
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <a className="navbar-brand" href = "/">Video Application</a>
                    </div>
                    <ul className="nav navbar-nav">
                        <li><a href="/">Home page</a></li>
                        {sessionStorage.getItem("role") === "Administrator" && <li><a href="UM"> User management </a></li>}
                        {sessionStorage.getItem("token") && <li><a href={"/user/"+sessionStorage.getItem("user")}> Profile </a></li>}
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        {!sessionStorage.getItem("token") && <li><a href="/register"><span className="glyphicon glyphicon-user"></span> Sign Up</a></li>}
                        <li><a href="/login" onClick={this.handleLogOut}><span className="glyphicon glyphicon-log-in"></span> {sessionStorage.getItem("token")? "Log out":"Log in" } </a></li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default NavigationBar