export const registerFormStyle = { 
    margin: "auto",
    width: "70%"
}

export const videoStyle = {
    textAlign:"center",
    border:"1px solid gray",
    width:"300px",
    height:"280px",
    padding:"10px 10px 10px 10px",
    margin:"5px 5px 5px 5px",
    float:"left",
    position:"relative"
}

export const videoPictureStyle = {
    position:"absolute",
    bottom:"10px",
    left:"10px"
}

export const videoStyleDate = {
    position: "absolute",
    bottom: 0,
    left: 70,
    color:"#A0A0A0"
}

export const videoStyleViews = {
    position: "absolute",
    bottom: 15,
    left: 70,
    color:"#A0A0A0"
}

export const videoStyleUser = {
    position: "absolute",
    bottom: 30,
    left: 70,
    color:"#A0A0A0"
}

export const subscribeButtonStyle = {
    float:"right",
    position:"relative",
    right:"20px"
}

export const newCommentButtonStyle = {
    marginTop:"15px",
    position:"relative",
    left:"80%"
}

export const commentsStyle = {
    backgroundColor:"lightcyan",
    float:"right",
    width:"42%",
    minHeight:"862px",
    maxHeight:"100%",
    borderStyle:"ridge",
    padding:"10px",
    marginRight:"10px"
}

export const commentsStyle1 = {
    overflow:"auto",
    overflowX:"hidden",
    height:"844px"
}

export const commentsDisabledStyle = {
    color:"#A8A8A8",
    position:"relative",
    left:"20px"
}

export const videoSettingsStyle ={
    float:"right",
    position:"relative",
    right:"20px"
}

export const videoDescriptionStyle = {
    backgroundColor:"lightcyan",
    borderStyle:"ridge",
    height:"151px",
    marginTop:"15px",
    padding:"10px"
}

export const videoDescriptionStyle1 = {
    overflow:"auto",
    overflowX:"hidden"
}

export const noDescriptionStyle = {
    color:"#A8A8A8",
    position:"relative",
    left:"20px"
}

export const loginFormStyle = {
    margin: "auto",
    width: "70%"
}

export const accountEditFormStyle1 = {
    position: "fixed",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "auto",
    backgroundColor: "rgba(0,0,0, 0.5)"
}

export const accountEditFormStyle2 = {
    position: "absolute",
    left: "30%",
    right: "30%",
    top: "10%",
    bottom: "10%",
    margin: "auto",
    background: "white",
    overflow:"auto",
    overflowX:"hidden"
}

export const addVideoFormStyle = {
    position: "fixed",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "auto",
    backgroundColor: "rgba(0,0,0, 0.5)"
}

export const addVideoFormStyle1 = {
    position: "absolute",
    left: "30%",
    right: "30%",
    top: "10%",
    bottom: "10%",
    margin: "auto",
    background: "white",
    overflow:"auto",
    overflowX:"hidden"
}

export const editVideoFormStyle = {
    position: "fixed",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "auto",
    backgroundColor: "rgba(0,0,0, 0.5)"
}

export const editVideoFormStyle1 = {
    position: "absolute",
    left: "37%",
    right: "37%",
    top: "20%",
    bottom: "15%",
    margin: "auto",
    background: "white",
    overflow:"auto",
    overflowX:"hidden"
}

export const homePageStyle = {
    float:"right",
    width:"83.34%"
}

export const homePageStyle1 = {
    marginLeft:10,
    width:"15.625%",
    height:"100%",
    position:"fixed"
}

export const userPageStyle1 = {
    width:"40%",
    height:"300px",
    marginLeft:"2%",
    marginTop:"30px"
}

export const userPageStyle2 = {
    backgroundColor:"lightcyan",
    width:"50%",
    height:"434px",
    borderStyle:"ridge",
    float:"right",
    marginRight:"4%",
    overflow:"auto",
    overflowX:"hidden"
}

export const userPageASStyle = {
    float:"right",
    position:"relative",
    right:"20px"
}

export const userPageStyle3 = {
    backgroundColor:"lightcyan",
    borderStyle:"ridge",
    height:"177px",
    marginTop:"45px",
    padding:"10px",
    overflow:"auto",
    overflowX:"hidden"
}

export const subscribingStyle1= {
    overflow:"auto",
    overflowX:"hidden"
}

export const subscribingStyle2 = {
    float:"right",
    marginRight:"20px"
}

export const AddPictureStyle = {
    position: "fixed",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    margin: "auto",
    backgroundColor: "rgba(0,0,0, 0.5)"
}

export const AddPictureStyle1 = {
    allign:"center",
    position: "absolute",
    left: "35%",
    right: "35%",
    top: "20%",
    bottom: "64%",
    margin: "auto",
    padding:"10px",
    background: "white",
    overflow:"auto",
    overflowX:"hidden"
}



