import React from "react"

function Picture (props) {
    return(
        <img src={`data:image/jpeg;base64,${props.data}`} alt="img" height={props.height} width={props.width} style={{borderRadius:props.radius}} />
    )
}

export default Picture