import React from "react"
import Video from "./Video"
import FilterForm from "./FilterForm"
import { homePageStyle, homePageStyle1 } from "./Styles"
import "./Loader.css"


class HomePage extends React.Component {
    constructor(){
        super()
        this.state = {
            videos:""
        }
    }


    componentDidMount(){
        fetch("https://localhost:44323/api/Videos",{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
        .then(response => response.json())
        .then(data => {
            this.setState({videos:data.map(x => <Video key={x.Id} id={x.Id} thumbnail={x.Thumbnail} name={x.Name} user={x.User} views = {x.Views} date = {x.Date} userPicture={x.UserPicture} />)})  
        })
    }

    getData = (childData) => {
        this.setState({videos:childData.map(x => <Video key={x.Id} id={x.Id} thumbnail={x.Thumbnail} name={x.Name} user={x.User} views = {x.Views} date = {x.Date} userPicture={x.UserPicture} />)})
    }


    render() {
            return(
                <div>
                    <div style={homePageStyle}>
                        {this.state.videos === ''? <div className="loader" style={{marginLeft:"35%",marginTop:"10%"}}> </div> : this.state.videos}
                    </div>
                    <div style={homePageStyle1}>
                        <FilterForm sendData={this.getData}/>
                    </div>
                </div>
            ) 
        }
    }

export default HomePage