import React from "react"
import axios from "axios"

class FilterForm extends React.Component{
    constructor(){
        super()
        this.state = {
            name:"",
            user:"",
            viewsFrom:"",
            viewsTo:"",
            dateFrom:"",
            dateTo:"",
            orderBy:""
        }
    }

    handleChange = (event) => {
        const {value,name} = event.target

        this.setState({[name]:value})
    }

    handleSubmit = (event) => {
        event.preventDefault()

        var filterObject = {
            Name: this.state.name,
            User: this.state.user,
            ViewsStart: this.state.viewsFrom,
            ViewsStop: this.state.viewsTo,
            DateStart: this.state.dateFrom,
            DateStop: this.state.dateTo,
            OrderBy: this.state.orderBy
          }

          axios("https://localhost:44323/api/Filter",{method:"POST",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")},data:filterObject})
          .then(data => {
              this.props.sendData(data.data)
          })
    }

    render() {
        return(
            <form onSubmit = {this.handleSubmit} >
                <h3> Filters: </h3>
                <div className="form-group">
                    <label>Name:</label>
                    <input  type="text"
                            className="form-control"
                            name="name"
                            onChange={this.handleChange}
                            value={this.state.name}
                            />
                </div>
                <div className="form-group">
                    <label>User:</label>
                    <input  type="text"
                            className="form-control"                                   
                            name="user"
                            onChange={this.handleChange}
                            value={this.state.user} 
                            />
                </div>
                <div className="form-group">
                    <label>Views from:</label>
                    <input  type="number"
                            className="form-control"                                  
                            name="viewsFrom"
                            onChange={this.handleChange}
                            value={this.state.viewsFrom} 
                            />
                </div>
                <div className="form-group">
                    <label>Views to:</label>
                    <input  type="number"
                            className="form-control"                                       
                            name="viewsTo"
                            onChange={this.handleChange}
                            value={this.state.viewsTo} 
                            />
                </div>
                <div className="form-group">
                    <label>Date from:</label>
                    <input  type="date"
                            className="form-control"                                       
                            name="dateFrom"
                            onChange={this.handleChange}
                            value={this.state.dateFrom} 
                            />
                </div>
                <div className="form-group">
                    <label>Date to:</label>
                    <input  type="date"
                            className="form-control"                                       
                            name="dateTo"
                            onChange={this.handleChange}
                            value={this.state.dateTo} 
                            />
                </div>
                <div className="form-group">
                    <label>Order by:</label>
                    <select className="form-control"                                       
                            name="orderBy"
                            onChange={this.handleChange}
                            value={this.state.orderBy} 
                            >
                                <option value="Name"> Name </option>
                                <option value="NameDesc"> Name descending </option>
                                <option value="User"> User </option>
                                <option value="UserDesc"> User descending </option>
                                <option value="Views"> Views </option>
                                <option value="ViewsDesc"> Views descending </option>
                                <option value="Date"> Date </option>
                                <option value="DateDesc"> Date descending </option>
                    </select>
                </div>
                <button type="submit" className="btn btn-default">Filter</button>
            </form>    
        )
    }
}

export default FilterForm