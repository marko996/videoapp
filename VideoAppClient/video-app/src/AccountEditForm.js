import React from "react"
import axios from "axios"
import {accountEditFormStyle1, accountEditFormStyle2} from "./Styles"


class AccountEditForm extends React.Component{
    constructor(){
        super()
        this.state = {
            id:"",
            name:"",
            lastName:"",
            description:"",
            password:"",
            newPassword:"",
            confirmPassword:""
        }
    }

    handleChange = (event) => {

        const {name,value,type,checked} = event.target
  
        type === "checkbox"? this.setState({[name]:checked}):this.setState({[name]:value})
      }

      componentDidMount() {
        fetch("https://localhost:44323/api/Users",{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
        .then(response => response.json())
        .then(data => {this.setState({id:data.Id,name:data.Name,lastName:data.LastName,description:data.Description})})

    }

      handleSaveChanges = (event) => {
            event.preventDefault()

            const data = {
                Id:this.state.id,
                Name:this.state.name,
                lastName:this.state.lastName,
                description:this.state.description
            }

            axios("https://localhost:44323/api/Users",{
                        method:"PUT",
                        headers:{Authorization:"Bearer "+sessionStorage.getItem("token")},
                        data:data                    
                    })
                    .then(this.props.closePopup)
                    .catch(() => {alert("Failed!")})
        }

      handleReset = (event) => {
          event.preventDefault()

          const data = {
            OldPassword:this.state.password,
            NewPassword:this.state.newPassword,
            ConfirmPassword: this.state.confirmPassword
          }

        axios("https://localhost:44323/api/Account/ChangePassword",{
                    method:"POST",
                    headers:{Authorization:"Bearer "+sessionStorage.getItem("token")},
                    data:data                    
                })
                .then(this.props.closePopup)
                .catch(() => {alert("Failed!")})
      }

    render() {
      return (
        <div>    
            <div style={accountEditFormStyle1}>
            <div style={accountEditFormStyle2}>
                <h3 style={{position:"relative",left:"40%"}}> Account settings </h3>
                <div style={{padding:"20px"}}>
                    <form>
                        <div className="form-group">
                                        <label>Name:</label>
                                        <input
                                                className="form-control"
                                                name="name"
                                                onChange={this.handleChange}
                                                value={this.state.name}
                                                />
                        </div>
                        <div className="form-group">
                                    <label>Last Name:</label>
                                    <input
                                            className="form-control"
                                            name="lastName"
                                            onChange={this.handleChange}
                                            value={this.state.lastName}
                                            />
                        </div>
                        <div className="form-group">
                                    <label>Description:</label>
                                    <textarea
                                            style={{resize:"none",height:"120px"}}
                                            className="form-control"
                                            name="description"
                                            onChange={this.handleChange}
                                            value={this.state.description}
                                            />
                            </div>
                        <button className="btn btn-warning" style={{float:"left"}} onClick={this.handleSaveChanges}> Save changes </button>
                        <br/>
                        <h3 style={{position:"relative",left:"40%"}}> Reset password</h3>
                        <div className="form-group">
                                        <label>Password:</label>
                                        <input
                                                className="form-control"
                                                type="password"
                                                name="password"
                                                onChange={this.handleChange}
                                                value={this.state.password}
                                                />
                        </div>
                        <div className="form-group">
                                        <label>New password:</label>
                                        <input
                                                className="form-control"
                                                type="password"
                                                name="newPassword"
                                                onChange={this.handleChange}
                                                value={this.state.newPassword}
                                                />
                        </div>
                        <div className="form-group">
                                        <label>Confirm password:</label>
                                        <input
                                                className="form-control"
                                                type="password"
                                                name="confirmPassword"
                                                onChange={this.handleChange}
                                                value={this.state.confirmPassword}
                                                />
                            </div>
                        <button className="btn btn-warning" style={{float:"left"}} onClick={this.handleReset}> Reset </button>  
                    </form> 
                    <br/> 
                    <br/>
                    <button className="btn btn-primary" style={{float:"right"}} onClick={this.props.closePopup}> Cancel </button>             
                </div>
            </div>
            </div>
        </div>
      
    )
    
    }
}

export default AccountEditForm