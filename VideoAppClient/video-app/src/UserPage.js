import React from "react"
import Subscribing from "./Subscribing"
import Video from "./Video"
import SubscribeButton from "./SubscribeButton"
import AddVideoForm from "./AddVideoForm"
import AccountEditForm from "./AccountEditForm"
import Picture from "./Picture"
import { userPageStyle2, userPageASStyle, userPageStyle3 } from "./Styles"
import AddPictureForm from "./AddPictureForm"

class UserPage extends React.Component{
    constructor(){
        super()
        this.state = {
            user:"",
            date:"",
            description:"",
            baned:"",
            subscribers:"",
            subscribing:"",
            videos:"",
            picture:"",
            mount:"",
            showAddVideoForm:false,
            showAccountEdit:false,
            showAddPictureForm:false
        }
    }

    

    componentDidMount(){
            fetch("https://localhost:44323/api/Users?username="+this.props.match.params.id,{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
            .then(response => response.json())
            .then(data => {
                this.setState({user:data.UserName,date:data.Date,description:data.Description,baned:data.Baned,subscribers:data.Subscribers,picture:data.Picture,mount:true})  
            })

            fetch("https://localhost:44323/api/Subscriptions?user="+this.props.match.params.id,{method:"GET"})
            .then(response => response.json())
            .then(data => {
                this.setState({subscribing:data.map(x => <Subscribing key={x.Subscribed} user={x.Subscribed} subscribers={x.Subscribers} />)})  
            })

            fetch("https://localhost:44323/api/VideosByUser?user="+this.props.match.params.id,{method:"GET",headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}})
            .then(response => response.json())
            .then(data => {
                this.setState({videos:data.map(x => <Video key={x.Id} id={x.Id} thumbnail={x.Thumbnail} name={x.Name} user={x.User} views = {x.Views} date = {x.Date} userPicture={x.UserPicture} />)})  
            })
    }

    handleClick = () => {
        this.setState({showAddPictureForm:true})
    }

    handleSubscribe = (childData) => {
        if(childData){
            this.setState(prevState => {return {subscribers:prevState.subscribers + 1}})
        }
        else {
            this.setState(prevState => {return {subscribers:prevState.subscribers - 1}})
        }
    }

    togglePopup = () => {
        this.setState({
          showAddVideoForm: !this.state.showAddVideoForm
        })
        this.componentDidMount()
      }

      togglePopup1 = () => {
        this.setState({
          showAccountEdit: !this.state.showAccountEdit
        })
        this.componentDidMount()
      }

      togglePopup2 = () => {
        this.setState({
          showAddPictureForm: !this.state.showAddPictureForm
        })
        this.componentDidMount()
      }
   
      

    render() {    
        return(
            <div>
                <div style={{marginRight:"56px"}}>
                    {this.state.user === sessionStorage.getItem("user")
                     &&
                    <button className="btn btn-success" style={{float:"left",marginLeft:"40px"}} onClick={this.togglePopup}> Add new video </button>}

                    {this.state.user === sessionStorage.getItem("user")
                     &&
                    <button className="btn btn-primary" style={{float:"left",marginLeft:"40px"}} onClick={this.handleClick}> Change profile picture </button>}

                    {this.state.mount && <SubscribeButton user={this.state.user} handleSubscribe={this.handleSubscribe} />}

                    {(this.state.user === sessionStorage.getItem("user") || sessionStorage.getItem("role") === "Administrator")
                     && 
                    <button className="btn btn-warning" style={userPageASStyle} onClick={this.togglePopup1}> Account settings  </button>}
                    <br/>
                    <br/>
                </div>
                <div>
                    <div style={userPageStyle2}>
                        <h3 style={{marginLeft:"20px",color:"#A8A8A8"}}> Subscribing: </h3>
                        <hr/>
                        {this.state.subscribing}
                    </div>
                    <div style={{width:"42%",height:"434px",marginLeft:"36px"}}>
                        <div style={{float:"left",marginLeft:"20px"}}>
                            <Picture data={this.state.picture} height="240px" width="240px"/>
                        </div>
                        <div style={{marginLeft:"40%"}}>
                            <h2 style={{paddingTop:"20px"}}> <span style={{color:"#A8A8A8"}}> Username: </span> {this.state.user} </h2>
                            <h2> <span style={{color:"#A8A8A8"}}> Registration date: </span> {this.state.date} </h2>
                            <h2> <span style={{color:"#A8A8A8"}}>  Number of subscribers: </span> {this.state.subscribers} </h2>
                            <h2> <span style={{color:"#A8A8A8"}}>  Baned: </span> {this.state.baned?"Baned":"No"} </h2>
                        </div>
                        <div style={userPageStyle3}>
                            {this.state.description? <h4> {this.state.description} </h4> : <h3 style={{color:"#A8A8A8"}}> No description </h3>}
                        </div>
                    </div>
                    <hr/>
                        <h3 style={{marginLeft:"45%",color:"#A8A8A8"}}> User videos </h3>
                        <div style={{marginLeft:"30px"}}>
                            {this.state.videos}
                        </div>
                </div>
                {this.state.showAddVideoForm && <AddVideoForm togglePopup={this.togglePopup}/>}
                {this.state.showAccountEdit && <AccountEditForm closePopup={this.togglePopup1}/>}
                {this.state.showAddPictureForm && <AddPictureForm closePopup={this.togglePopup2}/>}
            </div>
        )
    }
}

export default UserPage