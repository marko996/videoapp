import React from "react"
import axios from "axios"
import { AddPictureStyle, AddPictureStyle1 } from "./Styles"

class AddPictureForm extends React.Component{
    constructor() {
        super()
        this.state = {
            selectedFile:null
        }
    }

    onChangeHandler=event=>{
        this.setState({
            selectedFile: event.target.files[0]
          })
    }

    handleSubmit = (event) =>{
        event.preventDefault()

        const data = new FormData()
        data.append('file', this.state.selectedFile)
            axios("https://localhost:44323/api/users/PostUserImage",{
            method:"POST",
            headers:{Authorization:"Bearer "+sessionStorage.getItem("token")},
            data:data               
        })
        .then(this.props.closePopup)
        .catch(() => {alert("Failed!")})
    }


      render(){
          return(
              <div style={AddPictureStyle}>
                  <div style={AddPictureStyle1}>
                        <form onSubmit = {this.handleSubmit} >
                        <div className="form-group">
                            <label>Choose an image:</label>
                            <input  type="file"
                                    className="form-control"
                                    name="file"
                                    onChange={this.onChangeHandler}
                                    style={{width:"60%"}}
                                    />
                        </div>
                        <br/>
                        <button className="btn btn-warning" style={{float:"left"}}> Upload </button>
                        </form>
                        <button className="btn btn-primary" style={{float:"right"}} onClick={this.props.closePopup}> Cancel </button>
                    </div>
                </div>
          )
      }
}

export default AddPictureForm