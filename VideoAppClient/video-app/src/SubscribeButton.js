import React from "react"
import axios from "axios"
import { subscribeButtonStyle } from "./Styles"

class SubscribeButton extends React.Component {
    constructor() {
        super()
        this.state = {
            loggedInUser:sessionStorage.getItem("user"),
            isSubscriber:"",
            token:sessionStorage.getItem("token")
        }
    }

    componentDidMount(){   
        if(this.state.token) {
            fetch("https://localhost:44323/api/Subscriptions?subscribed="+this.props.user,
            {
                method:"GET",
                headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}
            })
            .then(response => response.json())
            .then(data => {
                this.setState({isSubscriber:data})
            })  
        }
    } 

    handleSubscribeUnsubscribe = () => {

        const  subscription = {
            SubscribedUsername:this.props.user
        }

        if(this.state.isSubscriber){
            axios("https://localhost:44323/api/Subscriptions?subscribed="+this.props.user,{
                method:"DELETE",
                headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}
            }).then(() => {
                this.setState({isSubscriber:false})
            })
            
        }
        else{
            axios("https://localhost:44323/api/Subscriptions",{
                method:"POST",
                data:subscription,
                headers:{Authorization:"Bearer "+sessionStorage.getItem("token")}
            }).then(() => {
                 this.setState({isSubscriber:true})
            })                               
        }
    } 

    render() {
        return(
            this.state.loggedInUser !== this.props.user && 

                <button 
                    className="btn btn-primary" 
                    onClick={this.handleSubscribeUnsubscribe} 
                    style={subscribeButtonStyle}
                    > 
                        {this.state.isSubscriber? "Unsubscribe":"Subscribe" }
                </button>
        )
    }
}

export default SubscribeButton