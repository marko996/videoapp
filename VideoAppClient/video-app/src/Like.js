import React from "react"
import axios from "axios"


class Like extends React.Component{

    handleClick = () => {
        if(sessionStorage.getItem("token")){
            axios("https://localhost:44323/api/Likes",{
                method:"POST",
                headers:{Authorization:"Bearer "+sessionStorage.getItem("token")},
                data:{
                    IsLike:(this.props.like?true:false),
                    CommentId:this.props.commentId,
                    VideoId:this.props.videoId
                }
            }).then((data) => {console.log(data); this.props.liked(data.data)})
        }
    }

    render() {
        return(
            <button onClick={this.handleClick}>
                <i className={this.props.like?"fa fa-thumbs-o-up":"fa fa-thumbs-o-down"}
                    style={{fontSize:"20px"}}>
                </i> 
                {this.props.like? "Like":"Dislike"} 
            </button>
        )
    }
}

export default Like